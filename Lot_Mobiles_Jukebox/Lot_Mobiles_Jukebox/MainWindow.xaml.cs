﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Linq.Dynamic;
using LotMobilesAutoSync;
using System.Windows.Threading;
using System.Reflection;
using System.Configuration;
using LotMobilesAutoSync.Models;
using System.Data;


namespace Lot_Mobiles_Jukebox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            RefreshList();
        }

        private void RefreshList()
        {
            CboCateg1.ItemsSource = null;
            CboCateg2.ItemsSource = null;
            CboCateg3.ItemsSource = null;
            CboCateg4.ItemsSource = null;
            CboCateg5.ItemsSource = null;
            dgGridSongsList.ItemsSource = null;
            this.Cursor = Cursors.Wait;
            Apps.lotContext.Configuration.ValidateOnSaveEnabled = true;
            var data = Apps.lotContext.Brands.ToList();

            List<LotDBEntity.Models.VASTabOne> lstVasTabOne = Apps.lotContext.VASTabOne.ToList();
            LotDBEntity.Models.VASTabOne vastabone = new LotDBEntity.Models.VASTabOne() { Name = "FILE TYPE" };
            lstVasTabOne.Insert(0, vastabone);
            CboCateg1.SelectedIndex = 0;
            CboCateg1.ItemsSource = lstVasTabOne;

            List<LotDBEntity.Models.VASTabTwo> lstVasTabTwo = Apps.lotContext.VASTabTwo.ToList();
            LotDBEntity.Models.VASTabTwo vastabtwo = new LotDBEntity.Models.VASTabTwo() { Name = "CATEGORY" };
            lstVasTabTwo.Insert(0, vastabtwo);
            CboCateg2.SelectedIndex = 0;
            CboCateg2.ItemsSource = lstVasTabTwo;

            List<LotDBEntity.Models.VASTabThree> lstVasTabThree = Apps.lotContext.VASTabThree.ToList();
            LotDBEntity.Models.VASTabThree vastabthree = new LotDBEntity.Models.VASTabThree() { Name = "ARTIST" };
            lstVasTabThree.Insert(0, vastabthree);
            CboCateg3.SelectedIndex = 0;
            CboCateg3.ItemsSource = lstVasTabThree;

            List<LotDBEntity.Models.VASTabFour> lstVasTabFour = Apps.lotContext.VASTabFour.ToList();
            LotDBEntity.Models.VASTabFour vastabfour = new LotDBEntity.Models.VASTabFour() { Name = "MUSIC DIRECTOR" };
            lstVasTabFour.Insert(0, vastabfour);
            CboCateg4.SelectedIndex = 0;
            CboCateg4.ItemsSource = lstVasTabFour;

            List<LotDBEntity.Models.VASTabFive> lstVasTabFive = Apps.lotContext.VASTabFive.ToList();
            LotDBEntity.Models.VASTabFive vastabfive = new LotDBEntity.Models.VASTabFive() { Name = "ALBUM" };
            lstVasTabFive.Insert(0, vastabfive);
            CboCateg5.SelectedIndex = 0;
            CboCateg5.ItemsSource = lstVasTabFive;

            dgGridSongsList.ItemsSource = Apps.lotContext.ValueAddedServicesData.ToList();
            lstSelectedSongsList = new List<LotDBEntity.Models.ValueAddedServicesData>();
            lstClearSelectedList = new List<LotDBEntity.Models.ValueAddedServicesData>();
            this.Cursor = Cursors.Arrow;
        }

        List<string> lstfiles = null;
        BackgroundWorker bgCopyWorker = null;
        private void btnCopy_TouchDown_1(object sender, TouchEventArgs e)
        {
            try
            {
                if (bgCopyWorker == null)
                {
                    if (!string.IsNullOrEmpty(selectedPath.Text) && selectedPath.Text != "Please select path to copy items.")
                    {
                        bgCopyWorker = new System.ComponentModel.BackgroundWorker();
                        bgCopyWorker.DoWork += bgCopyWorker_DoWork;
                        bgCopyWorker.ProgressChanged += bgCopyWorker_ProgressChanged;
                        bgCopyWorker.RunWorkerCompleted += bgCopyWorker_RunWorkerCompleted;
                        bgCopyWorker.WorkerReportsProgress = true; 
                        bgCopyWorker.WorkerSupportsCancellation = true;

                        lstfiles = new List<string>();

                        lstSelectedSongsList.ForEach(c =>
                        {
                            string imgName = c.ItemLocation.Replace("pack://siteoforigin:,,,/Songs/", string.Empty);
                            lstfiles.Add(Utilities.storingPath + @"\Songs\" + imgName);
                        });

                        PrgLoadingBar.Visibility = Visibility.Visible;

                        this.IsEnabled = false;

                        bgCopyWorker.RunWorkerAsync();
                    }
                    else
                        MessageBox.Show("Please select path to copy items.", "Lot Mobiles", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Already progressing.... Please wait...", "Lot Mobiles", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                this.IsEnabled = false;

                if (ex.Message.ToString().Trim() == "Not Enough Space.")
                    MessageBox.Show(ex.Message, "Lot Mobiles", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        void fileCopy_progressChanged(object sender, EventArgs e)
        {
            bgCopyWorker.ReportProgress(1, sender);
        }

        void bgCopyWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            this.IsEnabled = true;
            PrgLoadingBar.Visibility = Visibility.Collapsed;
            //selectedSongsList.Items.Clear();
            bgCopyWorker = null;
            selectedSongsList.ItemsSource = null;
            lstSelectedSongsList.Clear();
        }

        void bgCopyWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            try
            {
                ProgressNotification progressNotify = e.UserState as ProgressNotification;
                PrgLoadingBar.Minimum = 0;
                PrgLoadingBar.Maximum = progressNotify.ProductsCount;
                PrgLoadingBar.Value = progressNotify.CurrentProduct;
                //lblLoadingText.Text = progressNotify.UpdatedMessage;
            }
            catch (Exception)
            {
                this.IsEnabled = true;
            }
        }

        void bgCopyWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            FileCopy fileCopy = new FileCopy(destPath, lstfiles);
            fileCopy.progressChanged += fileCopy_progressChanged;
            fileCopy.InitiateCopy();
        }

        private void JukeboxVdo_Loaded_1(object sender, RoutedEventArgs e)
        {
            JukeboxVdo.Play();
        }

        private void JukeboxVdo_MediaEnded_1(object sender, RoutedEventArgs e)
        {
            JukeboxVdo.Position = new TimeSpan(0, 0, 1);
            JukeboxVdo.Play();
        }

        private string destPath = string.Empty;
        private void btnSelectFolderPath_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            SelectFolderPath folderPath = new SelectFolderPath();
            folderPath.ShowDialog();
            selectedPath.Visibility = Visibility.Visible;
            if (folderPath.DialogResult.HasValue && folderPath.DialogResult.Value)
            {
                destPath = folderPath.SelectedImagePath;
                selectedPath.Text = "Selected Items were copied to the following specified folder: " + folderPath.SelectedImagePath;
            }
            else
                selectedPath.Text = "Please select path to copy items.";
        }
        List<LotDBEntity.Models.ValueAddedServicesData> lstSelectedSongsList = null;

        List<LotDBEntity.Models.ValueAddedServicesData> lstClearSelectedList = null;

        private void btnMove_TouchDown_1(object sender, TouchEventArgs e)
        {
            selectedSongsList.ItemsSource = null;
            lstClearSelectedList.Clear();
            ApplyFilter();
            if(lstSelectedSongsList != null)
                selectedSongsList.ItemsSource = lstSelectedSongsList;
        }

        private void btnRemove_TouchDown_1(object sender, TouchEventArgs e)
        {
            foreach (var item in lstClearSelectedList)
            {
                LotDBEntity.Models.ValueAddedServicesData valueAddData = (LotDBEntity.Models.ValueAddedServicesData)item;
                lstSelectedSongsList.Remove((LotDBEntity.Models.ValueAddedServicesData)valueAddData);
            }
            selectedSongsList.ItemsSource = null;
            if(lstSelectedSongsList!=null)
                selectedSongsList.ItemsSource = lstSelectedSongsList;
            //lstSelectedSongsList.Clear();
            lstClearSelectedList.Clear();
        }

        private void CboCateg1_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ApplyFilter();
        }
        private void CboCateg2_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ApplyFilter();
        }
        private void CboCateg3_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ApplyFilter();
        }

        private void CboCateg4_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ApplyFilter();
        }
        private void CboCateg5_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            ApplyFilter();
        }
        string mainExpr = string.Empty;
        private void ApplyFilter()
        {
            mainExpr = string.Empty;

            if (CboCateg1.SelectedIndex > 0)
            {
                LotDBEntity.Models.VASTabOne vastabOne = (LotDBEntity.Models.VASTabOne)CboCateg1.SelectedItem;
                if (!string.IsNullOrEmpty(mainExpr))
                    mainExpr += " and ";
                mainExpr += " VASTabOne.ID==" + vastabOne.ID;
            }
            if (CboCateg2.SelectedIndex > 0)
            {
                LotDBEntity.Models.VASTabTwo vastabTwo = (LotDBEntity.Models.VASTabTwo)CboCateg2.SelectedItem;
                if (!string.IsNullOrEmpty(mainExpr))
                    mainExpr += " and ";
                mainExpr += " VASTabTwo.ID==" + vastabTwo.ID;
            }
            if (CboCateg3.SelectedIndex > 0)
            {
                LotDBEntity.Models.VASTabThree vastabThree = (LotDBEntity.Models.VASTabThree)CboCateg3.SelectedItem;
                if (!string.IsNullOrEmpty(mainExpr))
                    mainExpr += " and ";
                mainExpr += " VASTabThree.ID==" + vastabThree.ID;
            }

            if (CboCateg4.SelectedIndex > 0)
            {
                LotDBEntity.Models.VASTabFour vastabFour = (LotDBEntity.Models.VASTabFour)CboCateg4.SelectedItem;
                if (!string.IsNullOrEmpty(mainExpr))
                    mainExpr += " and ";
                mainExpr += " VASTabFour.ID==" + vastabFour.ID;
            }

            if (CboCateg5.SelectedIndex > 0)
            {
                LotDBEntity.Models.VASTabFive vastabFive = (LotDBEntity.Models.VASTabFive)CboCateg5.SelectedItem;
                if (!string.IsNullOrEmpty(mainExpr))
                    mainExpr += " and ";
                mainExpr += " VASTabFive.ID==" + vastabFive.ID;
            }

            if (!string.IsNullOrEmpty(mainExpr))
            {
                List<LotDBEntity.Models.ValueAddedServicesData> lstValueAddedServices = Apps.lotContext.ValueAddedServicesData.AsQueryable().Where(mainExpr).ToList();
                dgGridSongsList.ItemsSource = null;
                dgGridSongsList.ItemsSource = lstValueAddedServices;
            }
            else
            {
                List<LotDBEntity.Models.ValueAddedServicesData> lstValueAddedServices = Apps.lotContext.ValueAddedServicesData.ToList();
                dgGridSongsList.ItemsSource = null;
                dgGridSongsList.ItemsSource = lstValueAddedServices;
            }
        }

        private void btnAdminSetting_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            LoadAdminLogin();
        }

        AdminLogin AdmLogin;
        private void LoadAdminLogin()
        {
            btnAdminSetting.Visibility = Visibility.Hidden;
            AdmLogin = new AdminLogin();
            Settings.Children.Clear();
            Settings.Children.Add(AdmLogin);
            AdmLogin.myEventCloseAdminSetting += AdmLogin_myEventCloseAdminSetting;
            AdmLogin.myEventLoginAdmin += AdmLogin_myEventLoginAdmin;
        }
        void AdmLogin_myEventCloseAdminSetting(object sender, EventArgs e)
        {
            btnAdminSetting.Visibility = Visibility.Visible;
            Settings.Children.Clear();
        }

        void AdmLogin_myEventLoginAdmin(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        AdminSettings AdmSetting;
        private void LoadAdminSettings()
        {
            AdmSetting = new AdminSettings();
            Settings.Children.Clear();
            Settings.Children.Add(AdmSetting);
            AdmSetting.myEventAppDetails += AdmSetting_myEventAppDetails;
            AdmSetting.myEventAppSync += AdmSetting_myEventAppSync;
            AdmSetting.myEventLogoutSettings += AdmSetting_myEventLogoutSettings;
        }

        void AdmSetting_myEventAppDetails(object sender, EventArgs e)
        {

        }

        void AdmSetting_myEventAppSync(object sender, EventArgs e)
        {
            LoadSync();
        }

        private void LoadSync()
        {
            try
            {
                SetAutoSync setsync = new SetAutoSync();
                Settings.Children.Clear();
                Settings.Children.Add(setsync);
                setsync.myEventBacktoAdminSettings += setsync_myEventBacktoAdminSettings;
                setsync.autoSyncRefresh += setsync_autoSyncRefresh;
            }
            catch (Exception ex)
            {

            }

        }

        void setsync_autoSyncRefresh(object sender, EventArgs e)
        {
            InitiateAutoSync();
        }

        DispatcherTimer timer = null;
        Configuration config;
        private void InitiateAutoSync()
        {
            try
            {
                
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
                if (Convert.ToBoolean(config.AppSettings.Settings["AutoSyncEnabled"].Value))
                {
                    if (timer != null)
                    {
                        timer.Stop();
                        timer = null;
                    }
                    DateTime fromDate = DateTime.Now;
                    DateTime fetchSyncDate = Convert.ToDateTime(DateTime.Today.ToShortDateString() + " " + config.AppSettings.Settings["AutoSyncTiming"].Value);

                    TimeSpan tspan = fetchSyncDate - fromDate;

                    if (tspan.Seconds < 0)
                    {
                        fetchSyncDate = Convert.ToDateTime(DateTime.Today.AddDays(1).ToShortDateString() + " " + config.AppSettings.Settings["AutoSyncTiming"].Value);
                        tspan = fetchSyncDate - fromDate;
                    }

                    if (timer == null)
                    {
                        timer = new DispatcherTimer();
                        timer.Interval = new TimeSpan(Convert.ToInt32(tspan.Hours), Convert.ToInt32(tspan.Minutes), Convert.ToInt32(tspan.Seconds));
                        timer.Tick -= timer_Tick;
                        timer.Tick += timer_Tick;
                        timer.Start();
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }

        BackgroundWorker bgWorker;
        string exceptionMsg = string.Empty;
        void timer_Tick(object sender, EventArgs e)
        {
            try
            {
                config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);

                PrgLoadingBar.Visibility = Visibility.Visible;
                if (bgWorker == null)
                {
                    bgWorker = new BackgroundWorker();
                    bgWorker.DoWork += bgWorker_DoWork;
                    bgWorker.ProgressChanged += bgWorker_ProgressChanged;
                    bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;
                    bgWorker.WorkerReportsProgress = true;
                    bgWorker.WorkerSupportsCancellation = true;
                    bgWorker.RunWorkerAsync();
                }
                timer.Stop();
            }
            catch (Exception ex)
            {

            }
        }

        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                ProgressNotification progressNotify = e.UserState as ProgressNotification;
                PrgLoadingBar.Minimum = 0;
                PrgLoadingBar.Maximum = progressNotify.ProductsCount;
                PrgLoadingBar.Value = progressNotify.CurrentProduct;
                //lblLoadingText.Text = progressNotify.UpdatedMessage;
            }
            catch (Exception ex)
            {

            }

        }

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!string.IsNullOrEmpty(exceptionMsg))
            {

            }
            else
                Apps.lotContext = new LotDBEntity.DBFolder.LotDBContext(Utilities.connectionString);

            PrgLoadingBar.Visibility = Visibility.Collapsed;
            bgWorker = null;
            timer.Stop();
            RefreshList();
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                LotMobilesAutoSync.AutoSync autoSync = new LotMobilesAutoSync.AutoSync(Utilities.storingPath);
                autoSync.syncProgressEvent += autosync_progressNotification;
                autoSync.ftpProgressEvent += autoSync_ftpProgressEvent;

                autoSync.StartDBSync(true);
                autoSync.StartFTPSync(true);
            }
            catch (Exception ex)
            {
                exceptionMsg = ex.Message;
            }
        }

        void autoSync_ftpProgressEvent(object sender, EventArgs e)
        {
            bgWorker.ReportProgress(1, sender);
        }

        void autosync_progressNotification(object sender, EventArgs e)
        {
            bgWorker.ReportProgress(1, sender);
        }

        void setsync_myEventBacktoAdminSettings(object sender, EventArgs e)
        {
            LoadAdminSettings();
        }

        void AdmSetting_myEventLogoutSettings(object sender, EventArgs e)
        {
            Settings.Children.Clear();
            btnAdminSetting.Visibility = Visibility.Visible;
        }

        private void dgGridSongsList_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        private void CheckBox_TouchDown_1(object sender, TouchEventArgs e)
        {
            CheckBox chkBox = sender as CheckBox;
            btnTouchedItem = chkBox;
            if (!isScrollMoved)
            {

            }
            else
                isScrollMoved = false;
        }

        private void btnClearAll_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            selectedSongsList.ItemsSource = null;
            lstSelectedSongsList.Clear();
            lstClearSelectedList.Clear();
            ApplyFilter();
        }

        private void chkClearSelectedList_TouchDown_1(object sender, TouchEventArgs e)
        {
            CheckBox chkBox1 = sender as CheckBox;
            if (chkBox1 != null)
            {
                LotDBEntity.Models.ValueAddedServicesData vasData = chkBox1.DataContext as LotDBEntity.Models.ValueAddedServicesData;
                if (chkBox1.IsChecked == false)
                {
                    //chkBox.IsChecked = true;                    
                    if (!lstClearSelectedList.Exists(c => c.ID == vasData.ID))
                    {
                        lstClearSelectedList.Add(vasData);
                    }
                }
                else
                {
                    //chkBox.IsChecked = false;
                    LotDBEntity.Models.ValueAddedServicesData vasMainData = lstClearSelectedList.Where(c => c.ID == vasData.ID).FirstOrDefault();
                    if (vasMainData != null)
                        lstClearSelectedList.Remove(vasMainData);
                }
            }   
        }

        CheckBox btnTouchedItem = null;
        TouchPoint touchScrollStartPoint;
        TouchPoint touchScrollEndPoint;
        bool isScrollMoved = false;
        private void scrollViewerListing_ScrollChanged_1(object sender, ScrollChangedEventArgs e)
        {
            Touch.FrameReported -= Touch_FrameReported;
            Touch.FrameReported += Touch_FrameReported;
            if (touchScrollStartPoint != null && touchScrollEndPoint != null && touchScrollStartPoint.Position.Y != touchScrollEndPoint.Position.Y)
            {
                isScrollMoved = true;
            }
        }

        void Touch_FrameReported(object sender, TouchFrameEventArgs e)
        {
            touchScrollEndPoint = e.GetPrimaryTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchDown_1(object sender, TouchEventArgs e)
        {
            btnTouchedItem = null;
            touchScrollStartPoint = e.GetTouchPoint(this);
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            touchScrollEndPoint = e.GetTouchPoint(this);
        }

        private void scrollViewerListing_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            if (touchScrollStartPoint != null && touchScrollEndPoint != null)
            {
                double diffValue = Math.Abs(touchScrollStartPoint.Position.Y - touchScrollEndPoint.Position.Y);
                if (diffValue <= 10)
                {
                    isScrollMoved = false;

                    //CheckBox chkBox = sender as CheckBox;
                    if (btnTouchedItem != null)
                    {
                        LotDBEntity.Models.ValueAddedServicesData vasData = btnTouchedItem.DataContext as LotDBEntity.Models.ValueAddedServicesData;
                        if (btnTouchedItem.IsChecked == false)
                        {
                            //chkBox.IsChecked = true;
                            if (!lstSelectedSongsList.Exists(c => c.ID == vasData.ID))
                            {
                                lstSelectedSongsList.Add(vasData);
                            }
                        }
                        else
                        {
                            //chkBox.IsChecked = false;
                            LotDBEntity.Models.ValueAddedServicesData vasMainData = lstSelectedSongsList.Where(c => c.ID == vasData.ID).FirstOrDefault();
                            if (vasMainData != null)
                                lstSelectedSongsList.Remove(vasMainData);
                        }
                    }    
                }
            }
        }

    }
}
