﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Lot_Mobiles_Jukebox
{
    public class SelectIconConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            LotDBEntity.Models.VASTabOne vasTabone = (LotDBEntity.Models.VASTabOne)value;
            if (string.Equals(vasTabone.Name, "IMAGES", StringComparison.OrdinalIgnoreCase) && System.Convert.ToString(parameter).Equals("I"))
                return Visibility.Visible;

            if (string.Equals(vasTabone.Name, "VIDEOS", StringComparison.OrdinalIgnoreCase) && System.Convert.ToString(parameter).Equals("V"))
                return Visibility.Visible;

            if (string.Equals(vasTabone.Name, "AUDIOS", StringComparison.OrdinalIgnoreCase) && System.Convert.ToString(parameter).Equals("S"))
                return Visibility.Visible;

            if (string.Equals(vasTabone.Name, "OTHERS", StringComparison.OrdinalIgnoreCase) && System.Convert.ToString(parameter).Equals("O"))
                return Visibility.Visible;
            else
                return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
