﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lot_Mobiles_Jukebox
{
    /// <summary>
    /// Interaction logic for AdminLogin.xaml
    /// </summary>
    public partial class AdminLogin : UserControl
    {
        public AdminLogin()
        {
            InitializeComponent();
        }


        public event EventHandler myEventCloseAdminSetting;
        public event EventHandler myEventLoginAdmin;

        private void btnLogin_TouchDown_1(object sender, TouchEventArgs e)
        {
            if (txtName.Text == "lotadmin" && txtPassword.Password == "adminlot@123")
            {
                myEventLoginAdmin(this, null);
            }
            else
            {
                MessageBox.Show("Please Enter Valid UserName and Password....", "LOT", MessageBoxButton.OK, MessageBoxImage.Information);
                txtName.Text = string.Empty;
                txtPassword.Password = string.Empty;
            }
        }

        private void btnCanelLogin_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventCloseAdminSetting(this, null);
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            txtName.Focus();
        }

        private void txtPassword_TouchEnter_1(object sender, TouchEventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Program Files\Common Files\Microsoft Shared\ink\TabTip.exe");
        }

        private void txtName_TouchEnter_1(object sender, TouchEventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Program Files\Common Files\Microsoft Shared\ink\TabTip.exe");
        }
    }
}
