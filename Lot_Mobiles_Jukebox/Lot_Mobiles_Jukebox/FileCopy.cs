﻿using LotMobilesAutoSync.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lot_Mobiles_Jukebox
{
    /// <summary>
    /// This cs file is used to copy the files to Removable Disk
    /// </summary>
    public class FileCopy
    {
        private List<string> copiedFiles;
        private string destinationPath;
        public event EventHandler progressChanged;
        public FileCopy(string destPath, List<string> files)
        {
            destinationPath = destPath;
            copiedFiles = files;
        }

        public void InitiateCopy()
        {
            try
            {
                if (CalculateFreeSpace())
                {
                    int i = 0;
                    foreach (string item in copiedFiles)
                    {
                        try
                        {
                            i++;
                            string songName = item.Remove(0, item.LastIndexOf("\\") + 1);
                            if (!destinationPath.EndsWith("\\"))
                                destinationPath += "\\";
                            System.IO.File.Copy(item, destinationPath + songName, true);
                            progressChanged(new ProgressNotification(copiedFiles.Count, i, "Copying File:" + songName), null);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }
                else
                {
                    throw new Exception("Not Enough Space.");
                }

            }
            catch (Exception)
            {
            }
        }

        private bool CalculateFreeSpace()
        {
            try
            {
                long totalLength = 0;
                FileInfo fileInfo = null;
                if (destinationPath != null)
                {
                    string selectedDrive = destinationPath.Substring(0, 2);
                    copiedFiles.ForEach(c =>
                    {
                        fileInfo = new FileInfo(c);
                        totalLength += fileInfo.Length;
                    });

                    foreach (DriveInfo c in DriveInfo.GetDrives().Where(c => c.DriveType == DriveType.Removable))
                    {
                        if (c.RootDirectory.Name == selectedDrive + "\\")
                        {
                            if (c.AvailableFreeSpace < totalLength)
                                return false;
                            else
                                return true;
                        }
                    }
                }
                return false;
                
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
