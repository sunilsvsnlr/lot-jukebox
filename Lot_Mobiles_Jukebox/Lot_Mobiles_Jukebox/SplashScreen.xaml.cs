﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using System.Windows.Shapes;

namespace Lot_Mobiles_Jukebox
{
    /// <summary>
    /// Interaction logic for SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen : Window
    {
        public SplashScreen()
        {
            InitializeComponent();
        }

        BackgroundWorker bgw = null;
        private void SplashScreenVdo_Loaded_1(object sender, RoutedEventArgs e)
        {
            SplashScreenVdo.Play();
        }

        private void SplashScreenVdo_MediaEnded_1(object sender, RoutedEventArgs e)
        {
            SplashScreenVdo.Position = new TimeSpan(0,0,1);
            SplashScreenVdo.Play();
        }     

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            bgw = new BackgroundWorker();
            bgw.WorkerReportsProgress = true;
            bgw.WorkerSupportsCancellation = true;
            bgw.DoWork += bgw_DoWork;
            bgw.ProgressChanged += bgw_ProgressChanged;
            bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
            bgw.RunWorkerAsync();
        }

        void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                //Write code here to load usercontrol or launch window
                System.Threading.Thread.Sleep(1000);
                MainWindow HomePage = new MainWindow();
                this.Hide();
                HomePage.ShowDialog();
                this.Close();               
            }
            catch (Exception) { }
        }

        void bgw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txtText.Text = e.UserState as string;
        }

        LotDBEntity.DBFolder.LotDBContext DbContext = null;
        void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            bgw.ReportProgress(1, "Initializing Database... Please wait...");
            DbContext = new LotDBEntity.DBFolder.LotDBContext(Utilities.connectionString);
            bgw.ReportProgress(1, "Loading Database... Please wait...");
            DbContext.Configuration.ValidateOnSaveEnabled = true;
            var data = DbContext.Brands.ToList();
            Apps.lotContext = DbContext;
            bgw.ReportProgress(1, "Verifying License Information... Please wait...");
        }

    }
}
