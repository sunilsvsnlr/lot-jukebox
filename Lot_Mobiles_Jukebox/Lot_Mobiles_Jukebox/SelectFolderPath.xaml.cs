﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Lot_Mobiles_Jukebox
{
    /// <summary>
    /// Interaction logic for SelectFolderPath.xaml
    /// </summary>
    public partial class SelectFolderPath : Window
    {
        private object dummyNode = null;
        public SelectFolderPath()
        {
            InitializeComponent();
        }

        public string SelectedImagePath { get; set; }
        private void foldersItem_SelectedItemChanged_1(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            TreeView tree = (TreeView)sender;
            TreeViewItem temp = ((TreeViewItem)tree.SelectedItem);

            if (temp == null)
                return;
            SelectedImagePath = "";
            string temp1 = "";
            string temp2 = "";
            while (true)
            {
                temp1 = temp.Header.ToString();
                if (temp1.Contains(@"\"))
                {
                    temp2 = "";
                }
                SelectedImagePath = temp1 + temp2 + SelectedImagePath;
                if (temp.Parent.GetType().Equals(typeof(TreeView)))
                {
                    break;
                }
                temp = ((TreeViewItem)temp.Parent);
                temp2 = @"\";
            }
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            List<DriveInfo> lstDriveInfo = DriveInfo.GetDrives().Where(c => c.DriveType == DriveType.Removable).ToList();

            foreach (DriveInfo item in lstDriveInfo)
            {
                TreeViewItem trItem = new TreeViewItem();
                trItem.Header = item.RootDirectory.Name;
                trItem.Tag = item.RootDirectory.Name;
                trItem.FontWeight = FontWeights.Normal;
                trItem.Items.Add(dummyNode);
                trItem.Expanded += trItem_Expanded;
                foldersItem.Items.Add(trItem);
            }
        }

        void trItem_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            if (item.Items.Count == 1 && item.Items[0] == dummyNode)
            {
                item.Items.Clear();
                try
                {
                    foreach (string s in Directory.GetDirectories(item.Tag.ToString()))
                    {
                        TreeViewItem subitem = new TreeViewItem();
                        subitem.Header = s.Substring(s.LastIndexOf("\\") + 1);
                        subitem.Tag = s;
                        subitem.FontWeight = FontWeights.Normal;
                        subitem.Items.Add(dummyNode);
                        subitem.Expanded += new RoutedEventHandler(trItem_Expanded);
                        item.Items.Add(subitem);
                    }
                }
                catch (Exception) { }
            }
        }

        private void btnCancel_TouchDown_1(object sender, TouchEventArgs e)
        {
            this.DialogResult = false;
        }

        private void btnOk_TouchDown_1(object sender, TouchEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
