﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;

namespace Lot_Mobiles_Jukebox
{
    public static class Utilities
    {
        public static string connectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=D:\Main Lot\LotDB.mdf;Integrated Security=True;Connect Timeout=30";
        public static string storingPath = @"D:\Main Lot";

        public static T DeSerializeGeneric<T>(string serializedObject) where T : new()
        {
            try
            {
                if (serializedObject == null)
                {
                    return default(T);
                }
                if (serializedObject.Length == 0)
                {
                    if (default(T) != null)
                        return default(T);
                    return Activator.CreateInstance<T>();
                }
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                using (StringReader reader = new StringReader(serializedObject))
                {
                    return (T)serializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static BitmapImage ConvertotImg(object value)
        {
            var byteArrayImage = value as byte[];

            if (byteArrayImage != null && byteArrayImage.Length > 0)
            {
                try
                {
                    var ms = new MemoryStream(byteArrayImage);

                    var bitmapImg = new BitmapImage();

                    bitmapImg.BeginInit();
                    bitmapImg.StreamSource = ms;
                    bitmapImg.EndInit();

                    return bitmapImg;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;
        }
    }
}
