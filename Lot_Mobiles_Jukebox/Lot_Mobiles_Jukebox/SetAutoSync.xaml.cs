﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lot_Mobiles_Jukebox
{
    /// <summary>
    /// Interaction logic for SetAutoSync.xaml
    /// </summary>
    public partial class SetAutoSync : UserControl
    {
        public SetAutoSync()
        {
            InitializeComponent();
        }

        public event EventHandler myEventBacktoAdminSettings;
        public event EventHandler autoSyncRefresh;
        Configuration config;
        private void btnBackSettings_TouchDown_1(object sender, TouchEventArgs e)
        {
            myEventBacktoAdminSettings(this, null);
        }

        private void btnSaveAutoSync_TouchDown_1(object sender, TouchEventArgs e)
        {
            if (CboHours.SelectedIndex > 0 && CboMinutes.SelectedIndex > 0)
            {
                string time = CboHours.Text + ":" + CboMinutes.Text;

                Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);

                config.AppSettings.Settings["AutoSyncTiming"].Value = time;
                config.AppSettings.Settings["AutoSyncEnabled"].Value = Convert.ToString(chkEnable.IsChecked);
                config.Save();
                autoSyncRefresh(this, null);
                myEventBacktoAdminSettings(this, null);
            }
            else
            {
                MessageBox.Show("Please select valid time.", "LOT", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            config = ConfigurationManager.OpenExeConfiguration(Assembly.GetEntryAssembly().Location);
            if (!string.IsNullOrEmpty(config.AppSettings.Settings["AutoSyncTiming"].Value))
            {
                string autoSyncTime = config.AppSettings.Settings["AutoSyncTiming"].Value;
                string[] splitedAutoSyncTime = autoSyncTime.Split(':');
                if (splitedAutoSyncTime.Length > 0)
                {
                    CboHours.Text = Convert.ToString(splitedAutoSyncTime[0]);
                    CboMinutes.Text = Convert.ToString(splitedAutoSyncTime[1]);
                }
                chkEnable.IsChecked = Convert.ToBoolean(config.AppSettings.Settings["AutoSyncEnabled"].Value);
            }
        }
    }
}
